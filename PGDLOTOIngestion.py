"""
Name: PGDLOTOIngestion.py
Version: 0.1 Date: 05/01/2020
Author: Charles Vincent
Description: This python program will process incoming LOTO files sent via FTP.
It will respond to an AWS S3 PUT event, read the file and create a JSON
formatted file as output, written to a ./Preprocessed subdirectory of the
S3 bucket. S3LambdaEventHandler is the primary entry point defined in
the AWS console. ProcessFile is where all the real work is done.

Incoming File name = PGD.FTPIncoming.LOTO.localplantname.datetime.csv
Ingestion Lambda's are triggered by PGD.FTPIncoming.LOTO prefix.
Subsequent Lambdas are triggered by timer and will process output file --
PGD.FTPPreprocessed.LOTO.localplantname.datetime.json

Change Log:
05/01/2020 CBV Initial code.
"""

import boto3
import os
import sys
import uuid
from urllib.parse import unquote_plus
from botocore.exceptions import ClientError


def ProcessFile(InputFile, OutputFile):
    """This is the main routine where all the work is done. It is invoked by
    the Lambda event handler defined below. This routine will accept and
    input file and and output file destination as arguments. It will
    will grab event records and process them."""

    # set to return false by default since no file is created in the empty
    # shell.
    return False


def ResolveS3Exception(S3Exception):
    """
    This routine will parse the S3 Exception and return true if a retry
    is acceptable and false if a retry is not warranted. Additionally
    it will call the error logging function as well and write the S3 error
    out to the stderr. It is important to remember that the AWS SDK already
    performs a retry automatically on many errors internally. There is a
    retry delay that can be adjusted generally. The principle issue though
    is that while S3 calls through the SDK appear synchronous, they are
    asynchronous under the covers and the timeout for the listener is 30
    seconds, which is an eternity usually but frequently missed on S3
    operations on large files.

    This should probably be in a library
    [Dan: yes, or simply do something like:

    if ErrorCode in ['403', '404', '405', '409', '503']:
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = False
    ]
    """

    ErrorCode = S3Exception.response['ResponseMetadata']['HTTPStatusCode']

    if ErrorCode == '404':
        # Bucket does not exist, or other unrecoverable access issue
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = False

    elif ErrorCode == '403':
        # Not authorized for the operation requested
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = False

    elif ErrorCode == '405':
        # Operation invalid on current object
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = False

    elif ErrorCode == '409':
        # Operation conflicts with other account activities
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = False

    elif ErrorCode == '503':
        # Timout or other recoverable issue, can try again
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = True

    else:
        LogError(S3Exception.response['ResponseMetadata']['HTTPStatusCode'])
        RetryOK = False
        return(RetryOK)


def LogError(ErrorMessage):
    """
    [Dan: Python has a standard logging module we can implement this when ready
    also, need to check into Lambda logging practices
    (e.g. stdout versus push up a message up to MSK...just very convenient)]
    This is a simple routine and here only to isolate the error logging
    function so that in the future something fancier and more impressive
    can be inserted with a minimum of fuss

    This should probably be in a library
    """
    print(ErrorMessage, file=sys.stderr)


def S3LambdaEventHandler(Event, Context):
    """
    Event Handler routine, this will be triggered by AWS and must be configured
    in the console. This will grab event records and process them. Event holds
    the event data and context holds information such as the remaining time for
    the lambda. We use context for time remaining at the moment.
    """
    LogError('*** Environment Variables ***')
    LogError(os.environ)
    LogError('*** Event Information ***')
    LogError(Event)

    # create an S3 handler for the S3 operations.
    # All S3 opererations are handled in this routine. IO in the other routines
    # is local file system only
    S3Client = boto3.client('s3')

    for Record in Event['Records']:

        # Get Bucket Name from the event record
        Bucket = Record['s3']['bucket']['name']

        # Get the S3 Object Key and translate it from HTML %escaped and +
        # format to single char/space format
        Key = unquote_plus(Record['s3']['object']['key'])

        # Generate a temporary key for local tmp directory which is just the
        # bucket name sans slashes
        TmpKey = Key.replace('/', '')

        # create the temp path
        DownloadPath = '/tmp/{}{}'.format(uuid.uuid4(), TmpKey)

        # create local path for JSON file
        UploadPath = '/tmp/Preprocessed/'.format(TmpKey)

        # [Dan: do you need to create an ErrorPath here? Seems you use it
        # below]

        # The S3 operations are in a loop, to allow for retries. It is important
        # to remember that the AWS SDK already performs a retry automatically
        # on many errors internally. There is a retry delay that can be
        # adjusted generally. The principle issue though is that while S3 calls
        # through the SDK appear synchronous, they are asynchronous under the
        # covers and the timeout for the listener is 30 seconds, which is an
        # eternity usually but frequently missed on S3 operations
        # on large files. The connection timeout(request) and socket
        # timout(response) default for Pyhthon to 60 seconds. You can adjust
        # these by creating a config and passing it with the dowload request
        # as below....

        # class boto3.s3.transfer.TransferConfig(multipart_threshold=8388608,
        # max_concurrency=10,
        # multipart_chunksize=8388608,
        # num_download_attempts=5,
        # max_io_queue=100,
        # io_chunksize=262144,
        # use_threads=True)

        AllIsGood = True
        FileExists = False
        Retries = 0

        while AllIsGood:
            # Have a little optimism.....
            FileExists = True

            # now get the input file from S3
            try:
                S3Client.download_file(Bucket, Key, DownloadPath)

            # If a client error is thrown, then check that it was a 404 error.
            # If it was a 404 error, then the bucket does not exist.
            except ClientError as S3Exception:
                AllIsGood = ResolveS3Exception(S3Exception)

                if not AllIsGood:
                    AllIsGood = False
                    FileExists = False

            Retries += 1

            # if the file exists at this point in the loop
            # it means we are successful, and we can bug out
            if FileExists:
                break

            # alternatively a three time loser, we need to
            # hang it up and go home....
            if Retries > 3:
                break

        # Damn, looks like we did not successfully transfer the file to our
        # local filesystem to work on. The S3 Exception routine should write
        # out a detailed error message, but we should write out a backstop
        # message and exit the event handling routine.
        if not FileExists:
            LogError('100000S Error despooling incoming file to TMP directory')

        break

        # If the ProcessFile Routine cannot create an output file. then it
        # will return false

        # [Dan: this ErrorPath is undefined]
        AllIsGood = ProcessFile(DownloadPath, UploadPath, ErrorPath)

        # Same S3 retry loop as above, operations are in a loop, to allow for
        # retries. remember that the AWS SDK already
        # performs a retry automatically on many errors internally. There is a
        # retry delay that can be adjusted generally. The principle issue
        # though is that while S3 calls through the SDK appear synchronous,
        # they are asynchronous under the covers and the timeout for the
        # listener is 30 seconds, which is an eternity usually but frequently
        # missed on S3 operations on large files.

        # Reset our retry counter
        Retries = 0
        while AllIsGood:

            FileExists = True

            # now push the JSON formatted file to the bucket.
            try:
                S3Client.upload_file(Bucket, Key, UploadPath)

            # If a client error is thrown, then check that it was a 404 error.
            # If it was a 404 error, then the bucket does not exist.
            except ClientError as S3Exception:

                AllIsGood = ResolveS3Exception(S3Exception)
                if not ResolveS3Exception(S3Exception):
                    AllIsGood = False
                    FileExists = False
                    Retries += 1

            Retries += 1

        # if the file exists at this point in the loop it means we are
        # successful, and we can bug out
        if FileExists:
            break

        # if Retries > 2:
        # code can be inserted here to increase the timeout value
        # potentially in the future

        # alternatively, three time loser, we need to hang it up and go home...
        if Retries > 3:
            break

    # Any cleanup should be done here as well. Remember, Lambda
    # containers will be reused if multiple events are queued up and the tmp
    # directory will not always be empty, so we should clean up after ourselves
    # not only because it is polite, but also because there is a limit to the
    # space available in the local TMP directory and it could fill up with a
    # lot of large files coming in and bite us in the ass in production.

    try:
        os.remove(UploadPath)

        # [Dan: which 'path' is this supposed to be?]

        LogError(
            f"Removed temporary processed file {path} successfully"
        )

    except OSError as LocalError:
        LogError(
            f"Error deleting processed temp file, OS returned: {LocalError}"
        )

    try:
        os.remove(DownloadPath)

        # [Dan: which 'path' is this supposed to be?]

        LogError(
            f"Removed dowmloaded file {path} successfully"
        )

    except OSError as LocalError:
        LogError(
            f"Error deleting downloaded temp file, OS returned: {LocalError}"
        )

    # Currently S3 events will only send a single record. This code is here in
    # the event that that changes and more than one record can be returned,
    # or this code is used as the basis of a different event handler.
    # If multiple events are provided, it will be necessary to check the
    # remaining time for the Lambda to insure the operation can be completed
    # before grabbing the next record.
    # This will exit the main event handling loop.

    # if (context.get_remaining_time_in_millis < 3000):
    #     break
